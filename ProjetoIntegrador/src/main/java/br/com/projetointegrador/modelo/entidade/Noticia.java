
package br.com.projetointegrador.modelo.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author SOLIGO
 */
@Entity
@Table(name="tabela_noticia")
public class Noticia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name="nome_noticia", nullable = false)
    private String nomeNoticia;
    
    @Column(name="categoria_noticia")
    private String categoriaNoticia;
    
    @Column(name="data_noticia")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date data;
    
    @Lob
    @Column(name="descricao_noticia")
    private String descricaoNoticia;
    
    @Column(name="imagem_noticia")
    private String nomeImagem;
    
    public Long getId() {
        return id;
    }

    public String getNomeNoticia() {
        return nomeNoticia;
    }

    public void setNomeNoticia(String nomeNoticia) {
        this.nomeNoticia = nomeNoticia;
    }

    public String getDescricaoNoticia() {
        return descricaoNoticia;
    }

    public void setDescricaoNoticia(String descricaoNoticia) {
        this.descricaoNoticia = descricaoNoticia;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getCategoriaNoticia() {
        return categoriaNoticia;
    }

    public void setCategoriaNoticia(String categoriaNoticia) {
        this.categoriaNoticia = categoriaNoticia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Noticia)) {
            return false;
        }
        Noticia other = (Noticia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.projetointegrador.modelo.entidade.Noticia[ id=" + id + " ]";
    }

    public String getNomeImagem() {
        return nomeImagem;
    }

    public void setNomeImagem(String nomeImagem) {
        this.nomeImagem = nomeImagem;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
    
}
