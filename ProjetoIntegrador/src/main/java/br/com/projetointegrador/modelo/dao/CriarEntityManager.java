
package br.com.projetointegrador.modelo.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.*;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.jpa.internal.EntityManagerImpl;


public class CriarEntityManager {
    //Padrão Singleton: garante uma única instancia da classe
    private static CriarEntityManager criarem;
    private EntityManager em;
    
    public CriarEntityManager(){
      em=Persistence.createEntityManagerFactory("ProjetoIntegradorPU").createEntityManager();
    }
    
    public static CriarEntityManager getInstancia(){
      if(criarem==null){
           criarem=new CriarEntityManager(); 
    }
    return criarem;
}
     public EntityManager getEm(){
        return em;
     }
     
     public Connection getConnection(){
       EntityManagerImpl factory = (EntityManagerImpl) em;
            SessionFactoryImpl sessionFactoryImpl = (SessionFactoryImpl) factory.getSession().getSessionFactory();
        try {
            return sessionFactoryImpl.getConnectionProvider().getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(CriarEntityManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
     
     
}
