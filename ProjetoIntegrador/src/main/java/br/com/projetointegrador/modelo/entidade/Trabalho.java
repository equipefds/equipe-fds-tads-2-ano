/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projetointegrador.modelo.entidade;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author bruno
 */
@Entity
@Table(name = "tabela_trabalho")
public class Trabalho implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "titulo_trabalho", nullable = false)
    private String titulo;

    @Column(name = "nome_trabalho", nullable = false)
    private String nome;

    @Column(name = "tema_trabalho", nullable = false)
    private String tema;

    @Column(name = "descricao_trabalho")
    private String descricao;

    @Column(name = "autores_trabalho", nullable = false)
    private String autores;

    @Column(name = "caminho_trabalho", nullable = false)
    private String caminho;
    
    @Column(name = "versao_trabalho", nullable = false)
    private String versao;

    @Column(name = "VersaoFinal_trabalho", nullable = false, columnDefinition = "BOOLEAN")
    private boolean versaoFinal;
    
    @Lob
    @Column(name="Avaliacao_trabalho")
    private String avaliacao;

    @Column(name = "avaliado_trabalho", columnDefinition = "BOOLEAN")
    private boolean avaliado;

    @Column(name = "aprovado_trabalho", columnDefinition = "BOOLEAN")
    private boolean aprovado;

    @Column(name = "nota_trabalho")
    private String nota;

//    @OneToMany
//    private Avaliador avaliador;
   
    @ManyToOne
    @JoinColumn(name = "participante_id", nullable = false)
    private ParticipanteEvento participante;
    
    @ManyToOne
    @JoinColumn(name = "avaliador1_id")
    private Avaliador avaliador1;
    
//    @ManyToOne
//    @JoinColumn(name = "area_trabalho", nullable = false)
//    private Area area;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAutores() {
        return autores;
    }

    public void setAutores(String autores) {
        this.autores = autores;
    }

    public ParticipanteEvento getParticipante() {
        return participante;
    }

    public void setParticipante(ParticipanteEvento participante) {
        this.participante = participante;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getVersao() {
        return versao;
    }

    public void setVersao(String versao) {
        this.versao = versao;
    }

    public boolean isVersaoFinal() {
        return versaoFinal;
    }

    public void setVersaoFinal(boolean versaoFinal) {
        this.versaoFinal = versaoFinal;
    }

    public boolean isAvaliado() {
        return avaliado;
    }

    public void setAvaliado(boolean avaliado) {
        this.avaliado = avaliado;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public boolean isAprovado() {
        return aprovado;
    }

    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;
    }

    public String getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(String avaliacao) {
        this.avaliacao = avaliacao;
    }

//    public Area getArea() {
//        return area;
//    }
//
//    public void setArea(Area area) {
//        this.area = area;
//    }

    public Avaliador getAvaliador1() {
        return avaliador1;
    }

    public void setAvaliador1(Avaliador avaliador1) {
        this.avaliador1 = avaliador1;
    }

}
