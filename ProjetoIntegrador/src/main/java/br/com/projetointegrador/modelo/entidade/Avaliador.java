package br.com.projetointegrador.modelo.entidade;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author bruno
 */
@Entity
@Table(name = "tabela_avaliador")
public class Avaliador implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "quantidade_trabalhos_avaliador", nullable = false)
    private Integer quantidadeTrabalhos;

    @ManyToOne
    @JoinColumn(name = "usuario_avaliador", nullable = false)
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "evento_avaliador", nullable = false)
    private Evento evento;
    
    @ManyToOne
    @JoinColumn(name = "area_avaliador")
    private Area area;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Integer getQuantidadeTrabalhos() {
        return quantidadeTrabalhos;
    }

    public void setQuantidadeTrabalhos(Integer quantidadeTrabalhos) {
        this.quantidadeTrabalhos = quantidadeTrabalhos;
    }


}
