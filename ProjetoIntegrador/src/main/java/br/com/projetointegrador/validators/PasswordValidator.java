package br.com.projetointegrador.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator(value = "passwordValidator")
public class PasswordValidator implements Validator {

    int b = 0;
    int c = 0;
    int d = 0;
    

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value == null) {
            return;
        }

        String password = (String) value;
        
        for (int i = 0; i < password.length(); i++) {
            if ('A' <= password.charAt(i) && password.charAt(i) <= 'Z') // check if you have an uppercase
            {
                b++;
            }
            if ('a' <= password.charAt(i) && password.charAt(i) <= 'z') // check if you have a lowercase
            {
                c++;
            }
            if ('0' <= password.charAt(i) && password.charAt(i) <= '9') // check if you have a numeric
            {
                d++;
            }
        }

        if (password == null || password.equals("")) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "A senha não pode ser nula!", ""));
        }else if (b == 0) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "A senha deve ter no mínimo 1 letra maiúscula!", ""));
        }else if (c == 0) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "A senha deve ter no mínimo 1 letra minúscula!", ""));
        } else if (d == 0) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "A senha deve ter no mínimo 1 número!", ""));
        }else if (password.length() < 8) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "A senha deve ter no mínimo 8 caracteres!", ""));
        } else if (password.length() >= 22) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "A senha deve ter no máximo 22 caracteres!!!", ""));
        }
    }
}
