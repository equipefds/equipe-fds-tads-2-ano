package br.com.projetointegrador.controle;

import br.com.projetointegrador.modelo.dao.DAOGenerico;
import br.com.projetointegrador.modelo.entidade.Noticia;
import br.com.projetointegrador.modelo.entidade.Usuario;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.io.FilenameUtils;
import static org.apache.lucene.store.BufferedIndexInput.BUFFER_SIZE;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author SOLIGO
 */
@ManagedBean(name = "controleNoticia")
@SessionScoped
public class ControleNoticia implements Serializable {

    @ManagedProperty(value = "#{controleUsuario.usuarioLogado}")
    private Usuario user;
    DAOGenerico dao = new DAOGenerico();
    private Noticia noticia = new Noticia();
    private List<Noticia> listaNoticias = new ArrayList();
    private UploadedFile uploadFile;

    public void handleFileUpload(FileUploadEvent event) {

        uploadFile = event.getFile();

    }

    public ControleNoticia() {
    }

    public void cadastrarNoticia() {
        try (InputStream inputStream = uploadFile.getInputstream()) {

//            Glassfish
            Path folder = Paths.get(System.getProperty("com.sun.aas.instanceRoot"), "uploads", "noticias");

            if (!folder.toFile().exists()) {
                if (!folder.toFile().mkdirs()) {
                    folder = Paths.get(System.getProperty("com.sun.aas.instanceRoot"));
                }
            }

            String extension = "." + FilenameUtils.getExtension(uploadFile.getFileName());
            noticia.setNomeImagem(extension);
            dao.inserir(noticia);

            String filename = FilenameUtils.getBaseName(noticia.getId().toString());

            File targetFile = new File(folder + "/" + filename + extension);

            try (FileOutputStream fileOutputStream = new FileOutputStream(targetFile)) {
                byte[] buffer = new byte[BUFFER_SIZE];
                int bulk;
                while (true) {
                    bulk = inputStream.read(buffer);
                    if (bulk < 0) {
                        break;
                    }
                    fileOutputStream.write(buffer, 0, bulk);
                    fileOutputStream.flush();
                }
            }
            inputStream.close();
            noticia = new Noticia();

        } catch (IOException e) {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO", "Code of the error: DC1"));
        }

        redirecionamento("Deu bom!", "Noticia cadastrada com sucesso!", "/pages/admin/cadastro-noticia.xhtml");
        
    }
    
        public void redirecionamento(String tituloMensagem, String corpoMensagem, String linkMensagem){
        
        FacesContext faces = FacesContext.getCurrentInstance();

        faces.addMessage(null, new FacesMessage(tituloMensagem, corpoMensagem));
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + linkMensagem);
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        }
    
    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public Noticia getNoticia() {
        return noticia;
    }

    public void setNoticia(Noticia noticia) {
        this.noticia = noticia;
    }

    public List<Noticia> getListaNoticias() {
        try {
            if ((listaNoticias == null) || (listaNoticias.isEmpty())) {
                listaNoticias = dao.listar(Noticia.class);
            }
        } catch (Exception e) {

        }
        return listaNoticias;
    }

    public void setListaNoticias(List<Noticia> listaNoticias) {
        this.listaNoticias = listaNoticias;
    }

    public UploadedFile getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(UploadedFile uploadFile) {
        this.uploadFile = uploadFile;
    }
}