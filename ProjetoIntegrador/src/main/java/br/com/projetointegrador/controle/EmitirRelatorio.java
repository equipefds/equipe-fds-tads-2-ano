/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projetointegrador.controle;

import br.com.projetointegrador.util.ChamarRelatorio;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "emitirRelatorio")
@RequestScoped
public class EmitirRelatorio {

    /**
     * Creates a new instance of EmitirRelatorio
     */
    public EmitirRelatorio() {
        
    }
    
    public void emitir(){
        ChamarRelatorio ch = new ChamarRelatorio();
        
        //Caminho/nome do relatorio
        //parametros do relatorio
        //nome do pdf que vai ser gerado
        
        ch.imprimeRelatorio("Relatorio_Usuarios.jasper", null, "Relatorio-de-usuario");
    }
    
}
