/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projetointegrador.controle;

import br.com.projetointegrador.modelo.dao.DAOGenerico;
import br.com.projetointegrador.modelo.entidade.Evento;
import br.com.projetointegrador.modelo.entidade.ParticipanteEvento;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


/**
 *
 * @author igorr
 */
@ManagedBean(name = "controleExibirEvento")
@RequestScoped
public class ControleExibirEventos  {

    DAOGenerico dao = new DAOGenerico();
    private Evento evento = new Evento();
    private ParticipanteEvento participante = new ParticipanteEvento();
    private List<Evento> eventosFuturos = new ArrayList();
    private List<Evento> eventos = new ArrayList();
    private List<ParticipanteEvento> participantes = new ArrayList();


    public List<Evento> getEventos() {
        eventos = dao.listar(Evento.class);
        return eventos;
    }

    public void setEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }

    public List<Evento> getEventosFuturos() {

        try {
            if (eventosFuturos == null || eventosFuturos.isEmpty()) {

                if (eventos == null || eventos.isEmpty()) {
                    getEventos();

                    for (Evento e : eventos) {

                        if (e.getData().after(Date.from(Instant.now()))) {
                            eventosFuturos.add(e);
                        }
                    }
                }

            } else {
                
                for (Evento e : eventos) {

                    if (e.getData().after(Date.from(Instant.now()))) {
                        eventosFuturos.add(e);
                    }
                }
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
        return eventosFuturos;
    }

    public void setEventosFuturos(List<Evento> eventosFuturos) {
        this.eventosFuturos = eventosFuturos;
    }
    
    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public ParticipanteEvento getParticipante() {
        return participante;
    }

    public void setParticipante(ParticipanteEvento participante) {
        this.participante = participante;
    }

    public List<ParticipanteEvento> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(List<ParticipanteEvento> participantes) {
        this.participantes = participantes;
    }   

}
