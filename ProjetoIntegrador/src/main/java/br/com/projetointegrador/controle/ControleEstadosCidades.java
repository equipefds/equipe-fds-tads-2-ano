/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projetointegrador.controle;

import br.com.projetointegrador.modelo.dao.DAOGenerico;
import br.com.projetointegrador.modelo.entidade.Cidade;
import br.com.projetointegrador.modelo.entidade.Estado;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Admin
 */
@ApplicationScoped
@ManagedBean(name = "controleEstadosCidades")
public class ControleEstadosCidades {

    DAOGenerico dao = new DAOGenerico();
    private Estado estado = new Estado();
    private Cidade cidade = new Cidade();
    private List<Estado> estados = new ArrayList();
    private List<Cidade> cidades = new ArrayList();

    public ControleEstadosCidades() {
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Estado> getEstados() {
        try {
            if ((estados == null) || (estados.isEmpty())) {
                estados = dao.listar(Estado.class);
            }
        } catch (Exception e) {

        }
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    public List<Cidade> getCidades() {
        try {
            if ((cidades == null) || (cidades.isEmpty())) {
                cidades = dao.listar(Cidade.class);
            } 

        } catch (Exception e) {

        }
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }
}
