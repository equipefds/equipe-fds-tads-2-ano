/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projetointegrador.controle;

import br.com.projetointegrador.modelo.dao.DAOGenerico;
import br.com.projetointegrador.modelo.entidade.Cidade;
import br.com.projetointegrador.modelo.entidade.Endereco;
import br.com.projetointegrador.modelo.entidade.Estado;
import br.com.projetointegrador.modelo.entidade.Usuario;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "controleCadastro")
@ViewScoped
public class ControleCadastro {

    /**
     * Creates a new instance of ContrleCadastro
     */
    private Estado estado = new Estado();
    private Cidade cidade = new Cidade();
    Usuario usuario = new Usuario();
    DAOGenerico dao = new DAOGenerico();
    private Endereco endereco = new Endereco();
    @ManagedProperty(value = "#{controleEstadosCidades.estados}")
    private List<Estado> estados;
    @ManagedProperty(value = "#{controleEstadosCidades.cidades}")
    private List<Cidade> cidades;
    private List<Cidade> listaCidadesEstado = new ArrayList();
    private String senha;

    public ControleCadastro() {
    }

    public void cadastro() {
        for (Cidade cid : cidades) {
            if (Objects.equals(cid.getId(), cidade.getId())) {
                endereco.setCidade(cid);
            }

        }
        dao.inserir(endereco);

//        usuario.setNome(nome);
//        usuario.setCpf(cpf);
//        usuario.setEscolaridade(escolaridade);
//        usuario.setRg(rg);
//        usuario.setTelefone(telefone);
//        usuario.setEmail(email);
        usuario.setSenha(senha);
        usuario.setAtivo(true);
        usuario.setEndereco(endereco);
        usuario.setAutorizacao("USER");

        dao.inserir(usuario);

        FacesContext facescontext = FacesContext.getCurrentInstance();

        facescontext.addMessage(null, new FacesMessage("Sucesso", "Cadastro Realizado"));
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/login.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        this.senha = TransformaStringMD5.encrypt(senha);
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Estado> getEstados() {
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public List<Cidade> getListaCidadesEstado() {

        for (Cidade cid : cidades) {
            if (Objects.equals(cid.getEstado().getId(), this.estado.getId())) {
                listaCidadesEstado.add(cid);
            }

        }

        return listaCidadesEstado;
    }

    public void setListaCidadesEstado(List<Cidade> listaCidadesEstado) {
        this.listaCidadesEstado = listaCidadesEstado;
    }

}
