package br.com.projetointegrador.controle;

import br.com.projetointegrador.modelo.dao.DAOGenerico;
import br.com.projetointegrador.modelo.entidade.Avaliador;
import br.com.projetointegrador.modelo.entidade.Cidade;
import br.com.projetointegrador.modelo.entidade.Endereco;
import br.com.projetointegrador.modelo.entidade.Estado;
import br.com.projetointegrador.modelo.entidade.ParticipanteEvento;
import br.com.projetointegrador.modelo.entidade.Trabalho;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import javax.faces.bean.ManagedBean;
import br.com.projetointegrador.modelo.entidade.Usuario;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
/**
 *
 * @author Admin
 */
@ManagedBean(name = "controleUsuario")
@SessionScoped
public class ControleUsuario implements Serializable {
    /**
     * Creates a new instance of ControleUsuario
     */
    DAOGenerico dao = new DAOGenerico();
    @ManagedProperty(value = "#{controleEstadosCidades.estados}")
    private List<Estado> estados;
    @ManagedProperty(value = "#{controleEstadosCidades.cidades}")
    private List<Cidade> cidades;
    private Estado estado = new Estado();
    private Cidade cidade = new Cidade();
    private Usuario usuario = new Usuario();
    private Usuario usuarioLogado = new Usuario();
    private Endereco endereco = new Endereco();
    private List<Usuario> usuarios = new ArrayList();
    private List<Trabalho> trabalhos = new ArrayList();
    private List<Avaliador> avaliadores = new ArrayList();
    private List<ParticipanteEvento> participantes = new ArrayList();
    private List<Cidade> listaCidadesEstado = new ArrayList();
    private SecurityContext context = SecurityContextHolder.getContext();
    private String senha;

    public ControleUsuario() {

    }
    
    public void cadastro() {
        for (Cidade cid : cidades) {
            if (Objects.equals(cid.getId(), cidade.getId())) {
                endereco.setCidade(cid);
            }

        }
        dao.inserir(endereco);

        usuario.setSenha(senha);
        usuario.setAtivo(true);
        usuario.setEndereco(endereco);

        dao.inserir(usuario);

        usuario = new Usuario();
        endereco = new Endereco();
        redirecionamento("Sucesso!", "Cadastro Realizado", "/pages/admin/exibir-usuarios.xhtml");
        
    }

    public void alterar() {
        dao.alterar(usuarioLogado);
        redirecionamento("Sucesso!", "Cadastro Alterado", "/pages/admin/exibir-usuarios.xhtml");
        
    }

    public String acaoAlterar(Usuario usuario) {
        this.usuario = usuario;

        return "alterar-usuario";
    }

    public void remover(Usuario user) {

        this.usuario = user;
       
        
        for (Trabalho t : trabalhos) {
            if (t.getParticipante().getUsuario().equals(user)) {
                dao.remover(Trabalho.class, t.getId());
            }
        }
        
        for (Avaliador a : avaliadores) {
            if (a.getUsuario().equals(user)) {
                dao.remover(Avaliador.class, a.getId());
            }
        }

        for (ParticipanteEvento p : participantes) {
            if (p.getUsuario().equals(user)) {
                dao.remover(ParticipanteEvento.class, p.getId());
            }
        }

        dao.remover(Usuario.class, usuario.getId());

        usuario = new Usuario();
        redirecionamento("Sucesso!", "Cadastro Excluido", "/pages/admin/exibir-usuarios.xhtml");
    }
    
    public void redirecionamento(String tituloMensagem, String corpoMensagem, String linkMensagem){
        
        FacesContext faces = FacesContext.getCurrentInstance();

        faces.addMessage(null, new FacesMessage(tituloMensagem, corpoMensagem));
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + linkMensagem);
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        this.senha = TransformaStringMD5.encrypt(senha);
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Usuario> getUsuarios() {
        try {
                usuarios = dao.listar(Usuario.class);

        } catch (Exception e) {

        }
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public SecurityContext getContext() {
        return context;
    }

    public void setContext(SecurityContext context) {
        this.context = context;
    }

    public Usuario getUsuarioLogado() {
        Usuario u = new Usuario();
        String login = null;

        if (usuarios == null || usuarios.isEmpty()) {
            getUsuarios();
        }

        try {
            if (context instanceof SecurityContext) {
                Authentication authentication = context.getAuthentication();
                if (authentication instanceof Authentication) {
                    login = (((User) authentication.getPrincipal()).getUsername());

                }
            }

            for (Usuario user : usuarios) {
                if (user.getEmail().equals(login)) {
                    u = user;
                }
            }
            usuarioLogado = u;

        } catch (Exception e) {

        }

        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public List<Estado> getEstados() {
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public List<Cidade> getListaCidadesEstado() {

        for (Cidade cid : cidades) {
            if (Objects.equals(cid.getEstado().getId(), this.estado.getId())) {
                listaCidadesEstado.add(cid);
            }

        }

        return listaCidadesEstado;
    }

    public void setListaCidadesEstado(List<Cidade> listaCidadesEstado) {
        this.listaCidadesEstado = listaCidadesEstado;
    }

    public List<Trabalho> getTrabalhos() {
        try {
            if (trabalhos == null || trabalhos.isEmpty()) {
                trabalhos = dao.listar(Trabalho.class);

                return trabalhos;
            }

        } catch (Exception e) {
        }

        return trabalhos;
    }

    public void setTrabalhos(List<Trabalho> trabalhos) {
        this.trabalhos = trabalhos;
    }

    public List<Avaliador> getAvaliadores() {
        try {
            avaliadores = dao.listar(Avaliador.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return avaliadores;
    }

    public void setAvaliadores(List<Avaliador> avaliadores) {
        this.avaliadores = avaliadores;
    }

    public List<ParticipanteEvento> getParticipantes() {
        try {
            participantes = dao.listar(ParticipanteEvento.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return participantes;
    }

    public void setParticipantes(List<ParticipanteEvento> participantes) {
        this.participantes = participantes;
    }
}