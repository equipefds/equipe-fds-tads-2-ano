package br.com.projetointegrador.controle;

import br.com.projetointegrador.modelo.dao.DAOGenerico;
import br.com.projetointegrador.modelo.entidade.Area;
import br.com.projetointegrador.modelo.entidade.Avaliador;
import br.com.projetointegrador.modelo.entidade.ParticipanteEvento;
import br.com.projetointegrador.modelo.entidade.Trabalho;
import br.com.projetointegrador.modelo.entidade.Usuario;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.io.FilenameUtils;
import static org.apache.lucene.store.BufferedIndexInput.BUFFER_SIZE;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
/**
 *
 * @author Aluno
 */
@ManagedBean(name = "controleTrabalho")
@SessionScoped
public class ControleTrabalho implements Serializable {

    @ManagedProperty(value = "#{controleUsuario.usuarioLogado}")
    private Usuario user;
    private ParticipanteEvento participante = new ParticipanteEvento();
    private List<ParticipanteEvento> participantes = new ArrayList();
    private List<ParticipanteEvento> participantesAvaliadores = new ArrayList();
    private Trabalho trabalho = new Trabalho();
    private List<Trabalho> trabalhosParticipante = new ArrayList();
    private List<Trabalho> trabalhosAvaliador = new ArrayList();
    private List<Trabalho> trabalhos = new ArrayList();
    private List<Avaliador> avaliadores = new ArrayList();
    private Area area = new Area();
    private Area novaArea = new Area();
    private List<Area> areas = new ArrayList();
    private final DAOGenerico dao = new DAOGenerico();
    private UploadedFile uploadFile;
    private String folderToUpload;

    public void handleFileUpload(FileUploadEvent event) {
        uploadFile = event.getFile();
    }

    public void inserirTrabalho() {

        try (InputStream inputStream = uploadFile.getInputstream();) {

//          Glassfish
            Path folder = Paths.get(System.getProperty("com.sun.aas.instanceRoot"), "uploads", "eventos", "trabalhos");

            if (!folder.toFile().exists()) {
                if (!folder.toFile().mkdirs()) {
                    folder = Paths.get(System.getProperty("com.sun.aas.instanceRoot"));
                }
            }

            String extension = "." + FilenameUtils.getExtension(uploadFile.getFileName());
            trabalho.setCaminho(extension);
            trabalho.setParticipante(participante);
            trabalho.setVersao(incrementaVersao());
            if (trabalho.getVersao() == null) {
                trabalho.setVersao("0");
            }
            dao.inserir(trabalho);

            trabalho = new Trabalho();

            String filename = FilenameUtils.getBaseName(trabalho.getId().toString());

            File targetFile = new File(folder + "/" + filename + extension);

            try (FileOutputStream fileOutputStream = new FileOutputStream(targetFile)) {
                byte[] buffer = new byte[BUFFER_SIZE];
                int bulk;
                while (true) {
                    bulk = inputStream.read(buffer);
                    if (bulk < 0) {
                        break;
                    }
                    fileOutputStream.write(buffer, 0, bulk);
                    fileOutputStream.flush();
                }
            }
            inputStream.close();

        } catch (IOException e) {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO", "Code of the error: DC1"));
        }

        redirecionamento("Sucesso!", "Trabalho Enviado", "/pages/user/trabalhos-pendentes.xhtml");

    }

    public void acaoAvaliarTrabalho(Trabalho trabalho) {
        this.trabalho = trabalho;

        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/pages/user/avaliar-trabalho.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ControleTrabalho.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void avaliarTrabalho() {

        trabalho.setAvaliado(true);
        for (Avaliador a : avaliadores) {
            if (a.getUsuario().equals(user)) {
                trabalho.setAvaliador1(a);
            }
        }

        dao.alterar(trabalho);
        trabalho = new Trabalho();
        redirecionamento("Sucesso!", "Trabalho Avaliado", "/pages/user/trabalhos-pendentes.xhtml");

    }

    public String incrementaVersao() {

        Integer v = 0;
        Integer i;
        try {

            for (Trabalho t : trabalhosParticipante) {
                if (t.getTitulo().equals(trabalho.getTitulo()) || t.getAutores().equals(trabalho.getAutores())) {

                    i = Integer.parseInt(t.getVersao());
                    if (v < i) {
                        v = i;
                    }
                    String versao = v.toString();
                    return versao;
                } else {
                    String versao = v.toString();
                    return versao;
                }
            }

        } catch (Exception e) {
        }
        String versao = v.toString();
        return versao;
    }

    public Trabalho getTrabalho() {
        return trabalho;
    }

    public void setTrabalho(Trabalho trabalho) {
        this.trabalho = trabalho;
    }

    public List<Trabalho> getTrabalhosParticipante() {
        ArrayList lista = new ArrayList();
        try {
            if (trabalhosParticipante == null || trabalhosParticipante.isEmpty()) {

                for (Trabalho t : trabalhos) {
                    if (t.getParticipante() == participante) {
                        lista.add(t);
                    }
                }
                trabalhosParticipante = lista;
                
            } else {
                for (Trabalho t : trabalhos) {
                    if (t.getParticipante() == participante) {
                        lista.add(t);
                    }
                }
                trabalhosParticipante = lista;
            }
        } catch (Exception e) {
        }

        return trabalhosParticipante;
    }

    public void setTrabalhosParticipante(List<Trabalho> trabalhosParticipante) {
        this.trabalhosParticipante = trabalhosParticipante;
    }

    public String getFolderToUpload() {
        return folderToUpload;
    }

    public void setFolderToUpload(String folderToUpload) {
        this.folderToUpload = folderToUpload;
    }

    public UploadedFile getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(UploadedFile uploadFile) {
        this.uploadFile = uploadFile;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public ParticipanteEvento getParticipante() {
        try {
            if (participantes == null || participantes.isEmpty()) {
                getParticipantes();
            } else {
                for (ParticipanteEvento p : participantes) {
                    if (p.getUsuario() == user) {
                        participante = p;
                        return participante;
                    }
                }
            }

        } catch (Exception e) {
        }

        return participante;
    }

    public void setParticipante(ParticipanteEvento participante) {
        this.participante = participante;
    }

    public List<ParticipanteEvento> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(List<ParticipanteEvento> participantes) {
        this.participantes = participantes;
    }

    public List<Trabalho> getTrabalhos() {
        try {
            if (trabalhos == null || trabalhos.isEmpty()) {
                trabalhos = dao.listar(Trabalho.class);

                return trabalhos;
            }

        } catch (Exception e) {
        }

        return trabalhos;
    }

    public void setTrabalhos(List<Trabalho> trabalhos) {
        this.trabalhos = trabalhos;
    }

    public List<Trabalho> getTrabalhosAvaliador() {
        return trabalhosAvaliador;
    }

    public void setTrabalhosAvaliador(List<Trabalho> trabalhosAvaliador) {
        this.trabalhosAvaliador = trabalhosAvaliador;
    }

    public List<ParticipanteEvento> getParticipantesAvaliadores() {

        try {
            if (participantesAvaliadores == null || participantesAvaliadores.isEmpty()) {
                getParticipantes();

                for (ParticipanteEvento p : participantes) {
                    if (p.getPapel().getPapel().equals("Avaliador")) {
                        participantesAvaliadores.add(p);
                    }
                }
            } else {
                for (ParticipanteEvento p : participantes) {
                    if (p.getPapel().getPapel().equals("Avaliador")) {
                        participantesAvaliadores.add(p);
                    }
                }
            }

        } catch (Exception e) {
        }

        return participantesAvaliadores;
    }
    
    public void redirecionamento(String tituloMensagem, String corpoMensagem, String linkMensagem){
        
        FacesContext faces = FacesContext.getCurrentInstance();

        faces.addMessage(null, new FacesMessage(tituloMensagem, corpoMensagem));
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + linkMensagem);
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public void setParticipantesAvaliadores(List<ParticipanteEvento> participantesAvaliadores) {
        this.participantesAvaliadores = participantesAvaliadores;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Area getNovaArea() {
        return novaArea;
    }

    public void setNovaArea(Area novaArea) {
        this.novaArea = novaArea;
    }

    public List<Area> getAreas() {
        try {
            if (areas == null || areas.isEmpty()) {
                dao.listar(Area.class);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public List<Avaliador> getAvaliadores() {
        try {
            avaliadores = dao.listar(Avaliador.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return avaliadores;
    }

    public void setAvaliadores(List<Avaliador> avaliadores) {
        this.avaliadores = avaliadores;
    }

}
