/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projetointegrador.controle;

import br.com.projetointegrador.modelo.dao.DAOGenerico;
import br.com.projetointegrador.modelo.entidade.Avaliador;
import br.com.projetointegrador.modelo.entidade.Certificado;
import br.com.projetointegrador.modelo.entidade.Evento;
import br.com.projetointegrador.modelo.entidade.Papel;
import br.com.projetointegrador.modelo.entidade.ParticipanteEvento;
import br.com.projetointegrador.modelo.entidade.TipoEvento;
import br.com.projetointegrador.modelo.entidade.Trabalho;
import br.com.projetointegrador.modelo.entidade.Usuario;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.io.FilenameUtils;
import static org.apache.lucene.store.BufferedIndexInput.BUFFER_SIZE;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author igorr
 */
@ManagedBean(name = "controleEvento")
@SessionScoped
public class ControleEvento implements Serializable {

    @ManagedProperty(value = "#{controleUsuario.usuarioLogado}")
    private Usuario user;
    @ManagedProperty(value = "#{controleUsuario.usuarios}")
    private List<Usuario> usuarios;
    DAOGenerico dao = new DAOGenerico();
    private Evento evento = new Evento();
    private Papel papel = new Papel();
    private Avaliador avaliador = new Avaliador();
    private List<Papel> papeis = new ArrayList();
    private ParticipanteEvento participante = new ParticipanteEvento();
    private List<Evento> eventos = new ArrayList();
    private List<Evento> eventosFuturos = new ArrayList();
    private List<Evento> eventosTerminados = new ArrayList();
    private List<Avaliador> avaliadores = new ArrayList();
    private List<Avaliador> avaliadoresEvento = new ArrayList();
    private List<ParticipanteEvento> participantes = new ArrayList();
    private List<ParticipanteEvento> participantesEvento = new ArrayList();
    private List<Avaliador> participantesAvaliadores = new ArrayList();
    private List<Certificado> certificados = new ArrayList();
    private List<TipoEvento> tiposEvento = new ArrayList();
    private List<Trabalho> trabalhos = new ArrayList();
    private TipoEvento tipoEvento = new TipoEvento();
    private TipoEvento novoTipo = new TipoEvento();
    private String email;
    private UploadedFile uploadFile;
    private String folderToUpload;

    public void handleFileUpload(FileUploadEvent event) {

        uploadFile = event.getFile();

    }

    public void cadastrar() {
        if (novoTipo == null) {
            for (TipoEvento tipo : tiposEvento) {
                if (Objects.equals(tipo.getId(), tipo.getId())) {
                    evento.setTipoEvento(tipo);
                }

            }
        } else {
            dao.inserir(novoTipo);
            evento.setTipoEvento(novoTipo);
        }

        try (InputStream inputStream = uploadFile.getInputstream()) {

//            Glassfish
            Path folder = Paths.get(System.getProperty("com.sun.aas.instanceRoot"), "uploads", "eventos");

//            Tomcat
//            Path folder = Paths.get(System.getProperty("catalina.base"), "upload", "eventos", evento.getNomeDoEvento());
//            System.out.println(folder.toString());
//            Glassfish
            if (!folder.toFile().exists()) {
                if (!folder.toFile().mkdirs()) {
                    folder = Paths.get(System.getProperty("com.sun.aas.instanceRoot"));
                }
            }

            //            Tomcat
            //            if (!folder.toFile().exists()) {
            //                if (!folder.toFile().mkdirs()) {
            //                    folder = Paths.get(System.getProperty("catalina.base"));
            //                }
            //            }
            String extension = "." + FilenameUtils.getExtension(uploadFile.getFileName());
            evento.setNomeImagem(extension);
            evento.setVagasRestantes(evento.getTotalVagas());
            dao.inserir(evento);

            String filename = evento.getId().toString();

            File targetFile = new File(folder + "/" + filename + extension);

            try (FileOutputStream fileOutputStream = new FileOutputStream(targetFile)) {
                byte[] buffer = new byte[BUFFER_SIZE];
                int bulk;
                while (true) {
                    bulk = inputStream.read(buffer);
                    if (bulk < 0) {
                        break;
                    }
                    fileOutputStream.write(buffer, 0, bulk);
                    fileOutputStream.flush();
                }
            }
            inputStream.close();

            evento = new Evento();

        } catch (IOException e) {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO", "Code of the error: DC1"));
        }

        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Sucesso", "Evento cadastrado"));
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/pages/admin/exibir-eventos.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void alterar() {

        dao.alterar(evento);

        evento = new Evento();

        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Sucesso", "Evento Alterado"));
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/pages/admin/exibir-eventos.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void remover() {
        
        for (Certificado c : certificados) {
            if (c.getEvento().equals(evento)) {
                dao.remover(Certificado.class, c.getId());
            }
        }
        
        for (Trabalho t : trabalhos) {
            if (t.getParticipante().getEvento().equals(evento)) {
                dao.remover(Trabalho.class, t.getId());
            }
        }
        
        for (Avaliador a : avaliadores) {
            if (a.getEvento().equals(evento)) {
                dao.remover(Avaliador.class, a.getId());
            }
        }

        for (ParticipanteEvento p : participantes) {
            if (p.getEvento().equals(evento)) {
                dao.remover(ParticipanteEvento.class, p.getId());
            }
        }
        
        dao.remover(Evento.class, evento.getId());

        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Sucesso", "Evento excluido"));
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/pages/admin/exibir-eventos.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void inscreverEvento() {
        try {
            if (evento.getInicioInscricao().before(Date.from(Instant.now()))) {
                if (evento.getFinalInscricao().after(Date.from(Instant.now()))) {
                    if (evento.getVagasRestantes() != 0) {

                        participante.setEvento(evento);
                        participante.setPapel(papel);
                        participante.setUsuario(user);

                        dao.inserir(participante);

                        if (!"Avaliador".equals(participante.getPapel().getPapel())) {

                            int vagas = evento.getVagasRestantes();
                            evento.setVagasRestantes(vagas - 1);
                            dao.alterar(evento);
                        }

                        redireciona();
                    } else {

                        FacesContext context = FacesContext.getCurrentInstance();

                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falha", "Vagas Esgotadas"));
                    }
                } else {
                    FacesContext context = FacesContext.getCurrentInstance();

                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falha", "Inscrições Encerradas"));
                }
            } else {
                FacesContext context = FacesContext.getCurrentInstance();

                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falha", "As inscrições ainda não foram abertas"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void adcionarAvaliador() {
        try {
            Usuario usuario = null;
            for (Usuario u : usuarios) {
                if (u.getEmail().equals(email)) {
                    usuario = u;
                }
            }
            if (usuario == null) {
                FacesContext context = FacesContext.getCurrentInstance();

                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falha", "Usuário não cadastrado!"));
            }

            avaliador.setEvento(evento);
            avaliador.setUsuario(usuario);

            dao.inserir(avaliador);

            int vagas = evento.getVagasRestantes();
            evento.setVagasRestantes(vagas - 1);
            dao.alterar(evento);

            redireciona();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void acaoAlterar(Evento evento) {
        this.evento = evento;

        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/pages/admin/alterar-evento.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void acaoAvaliador() {

        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/pages/admin/adicionar-avaliador.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void acaoExcluir(Evento evento) {
        this.evento = evento;
        dao.remover(Evento.class, evento.getId());

        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Sucesso", "Evento excluido"));
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/pages/admin/exibir-eventos.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void acaoInscricao(Evento evento) {
        this.evento = evento;

        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/pages/user/inscricao-evento.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void acaoDetalhes(Evento evento) {
        this.evento = evento;

        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/pages/admin/detalhes-evento.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void redireciona() {

        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Sucesso", "Inscrição Concluida"));
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            if (participante.getPapel().getPapel().equals("apresentador")) {
                participante = new ParticipanteEvento();
                ec.redirect(ec.getRequestContextPath() + "/pages/user/enviar-trabalho.xhtml");
            } else {
                participante = new ParticipanteEvento();
                ec.redirect(ec.getRequestContextPath() + "/pages/user/exibir-eventos-futuros");

            }

        } catch (IOException ex) {
            Logger.getLogger(ControleEvento.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public List<Evento> getEventos() {
        eventos = dao.listar(Evento.class);
        return eventos;
    }

    public void setEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }

    public Papel getPapel() {
        for (Papel p : papeis) {
            if (Objects.equals(p.getId(), papel.getId())) {
                papel = p;
            } else {
                return papel;
            }
        }
        return papel;
    }

    public void setPapel(Papel papel) {
        this.papel = papel;
    }

    public ParticipanteEvento getParticipante() {
        try {

            getParticipantesEvento();

            for (ParticipanteEvento p : participantesEvento) {
                if (p.getUsuario().equals(user)) {
                    participante = p;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return participante;
    }

    public void setParticipante(ParticipanteEvento participante) {
        this.participante = participante;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public List
            getPapeis() {
        try {
            if (papeis == null || papeis.isEmpty()) {
                papeis = dao.listar(Papel.class
                );

            } else {
                return papeis;
            }
        } catch (Exception e) {

        }

        return papeis;
    }

    public void setPapeis(List papeis) {
        this.papeis = papeis;
    }

    public List<Evento> getEventosFuturos() {

        try {
            if (eventosFuturos == null || eventosFuturos.isEmpty()) {

                if (participante == null) {
                    getParticipante();

                    if (participante.getEvento().getData().after(Date.from(Instant.now()))) {
                        eventosFuturos.add(participante.getEvento());
                    }
                }

            } else if (participante.getEvento().getData().after(Date.from(Instant.now()))) {
                eventosFuturos.add(participante.getEvento());
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return eventosFuturos;
    }

    public void setEventosFuturos(List<Evento> eventosFuturos) {
        this.eventosFuturos = eventosFuturos;
    }

    public List<Evento> getEventosTerminados() {
        try {
            if (eventosTerminados == null || eventosTerminados.isEmpty()) {

                if (participante == null) {
                    getParticipante();

                    if (participante.getEvento().getData().before(Date.from(Instant.now()))) {
                        eventosTerminados.add(participante.getEvento());
                    }
                } else if (participante.getEvento().getData().before(Date.from(Instant.now()))) {
                    eventosTerminados.add(participante.getEvento());
                }

            }

        } catch (Exception e) {
            System.out.println(e);
        }

        return eventosTerminados;
    }

    public void setEventosTerminados(List<Evento> eventosTerminados) {
        this.eventosTerminados = eventosTerminados;
    }

    public List<ParticipanteEvento> getParticipantes() {
        try {
            participantes = dao.listar(ParticipanteEvento.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return participantes;
    }

    public void setParticipantes(List<ParticipanteEvento> participantes) {
        this.participantes = participantes;
    }

    public String getFolderToUpload() {
        return folderToUpload;
    }

    public void setFolderToUpload(String folderToUpload) {
        this.folderToUpload = folderToUpload;
    }

    public UploadedFile getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(UploadedFile uploadFile) {
        this.uploadFile = uploadFile;
    }

    public List getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List usuarios) {
        this.usuarios = usuarios;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<TipoEvento> getTiposEvento() {
        try {

            tiposEvento = dao.listar(TipoEvento.class);

        } catch (Exception e) {

        }
        return tiposEvento;
    }

    public void setTiposEvento(List<TipoEvento> tiposEvento) {
        this.tiposEvento = tiposEvento;
    }

    public TipoEvento getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(TipoEvento tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public TipoEvento getNovoTipo() {
        return novoTipo;
    }

    public void setNovoTipo(TipoEvento novoTipo) {
        this.novoTipo = novoTipo;
    }   

    public Avaliador getAvaliador() {
        return avaliador;
    }

    public void setAvaliador(Avaliador avaliador) {
        this.avaliador = avaliador;
    }

    public List<Avaliador> getParticipantesAvaliadores() {

        return participantesAvaliadores;
    }

    public void setParticipantesAvaliadores(List<Avaliador> participantesAvaliadores) {
        this.participantesAvaliadores = participantesAvaliadores;
    }

    public List<ParticipanteEvento> getParticipantesEvento() {

        try {
            if (participantesEvento == null || participantesEvento.isEmpty()) {

                getParticipantes();

                for (ParticipanteEvento p : participantes) {
                    if (p.getEvento().getId().equals(evento.getId())) {

                    }
                }

            } else {
                for (ParticipanteEvento p : participantes) {
                    if (p.getEvento().getId().equals(evento.getId())) {
                        participantesEvento.add(p);
                    }
                }
            }
        } catch (Exception e) {
        }

        return participantesEvento;
    }

    public void setParticipantesEvento(List<ParticipanteEvento> participantesEvento) {
        this.participantesEvento = participantesEvento;
    }

    public List<Avaliador> getAvaliadores() {
        try {
            avaliadores = dao.listar(Avaliador.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return avaliadores;
    }

    public void setAvaliadores(List<Avaliador> avaliadores) {
        this.avaliadores = avaliadores;
    }

    public List<Avaliador> getAvaliadoresEvento() {
        try {
            getAvaliadores();
            for (Avaliador a : avaliadores) {
                if (a.getEvento().equals(evento)) {
                    avaliadoresEvento.add(a);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return avaliadoresEvento;
    }

    public void setAvaliadoresEvento(List<Avaliador> avaliadoresEvento) {
        this.avaliadoresEvento = avaliadoresEvento;
    }

    public List<Certificado> getCertificados() {
        certificados = dao.listar(Certificado.class);
        return certificados;
    }

    public void setCertificados(List<Certificado> certificados) {
        this.certificados = certificados;
    }
    
    public List<Trabalho> getTrabalhos() {
        try {
            if (trabalhos == null || trabalhos.isEmpty()) {
                trabalhos = dao.listar(Trabalho.class);

                return trabalhos;
            }

        } catch (Exception e) {
        }

        return trabalhos;
    }

    public void setTrabalhos(List<Trabalho> trabalhos) {
        this.trabalhos = trabalhos;
    }

}
